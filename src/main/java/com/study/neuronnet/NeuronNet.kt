package com.study.neuronnet
import com.study.Loger
import kotlin.math.*

class NeuronNet {

    class NeuronNetException(message: String): Exception(message)

    val sinapsArray = ArrayList<Sinaps>()
    val neuronArray = ArrayList<Neuron>()

    var epsilon = 0.3
    var alfa = 0.5

    private var outNeuronCash = Neuron("CASH", 0, 0.0)
    private var rowsCash = 0

    val loger = Loger()

    /**
     *Задаем значения входных нейронов
     */
    fun setInVal(neuronName: String, neuronValue: Double)
    {
        neuronArray.forEach {
            if(neuronName == it.name){
                loger.log("Neuron ${it.name} set value $neuronValue")
                it.value = neuronValue
                return
            }
        }
        loger.log("Neuron ${neuronName} not found.")
    }

    /**
     *Запустить нейронную сеть
     */
    fun runNeuron(): Double
    {
        //loger.log("RUN NEURON")
        val rows = countRows()
        for(i in 1 until rows){
            //loger.log("Counting row $i")
            val neurons = getNeuronsFromRow(i)
            neurons.forEach {
                val inSnapses = getInSinapses(it)
                val value = countNeuronValue(inSnapses)
                it.value = value
                //loger.log("Neuron ${it.name} value ${it.value}")
                if(i == rows - 1)
                    return value
            }
        }
        throw NeuronNetException("Run neuron exception.")
    }

    /**
     *Обучить нейронную сеть.
     */
    fun teachNeuronNetItirarion(outIdeal: Double, outActual: Double)
    {
        //loger.log("TEACH NEURON NET ITIRATION")
        val outNeuron = getOutputNeuron()
        outNeuron.delta = sigmaOutput(outIdeal, outActual)
        //loger.log("Out neuron ( ${outNeuron.name}) delta: ${outNeuron.delta}")
        teachInNeuronSinapses(outNeuron)
        for(i in 0..outNeuron.row - 2){
            //loger.log("Row: $i")
            val neurons = getNeuronsFromRow((outNeuron.row -1) - i)
            neurons.forEach {
                //loger.log("Neuron sinaps study ${it.name}")
                teachInNeuronSinapses(it)
            }
        }
    }

    /**
     * Обучить все входящие синапсы нейрона
     */
    private fun teachInNeuronSinapses(neuron: Neuron)
    {
        val neuronSinapses = getInSinapses(neuron)
        neuronSinapses.forEach {
            val n =it.neuronIn
            n.delta = sigmaHidden(n)
            val grad = n.value * it.neuronOut.delta
            val dw = mor(grad, it.deltaValueLast)
            it.deltaValueLast = dw
            it.value = it.value + dw
            //loger.log("Sinaps (${it.name}) grad $grad dw $dw value ${it.value}")
        }
    }


    /*******МАТЕМАТИКА ОБУЧЕНИЕ НЕЙРОСЕТИ*******/
    /**
     *Производная функции активации
     */
    private  fun fSigmoid(out: Double) =
        (1.0 - out) * out

    /**
     *Производная функции активации
     */
    private  fun fTangh(out: Double) =
            1.0 - out * out

    /**
     *Расчет дельты для выходного нейрона
     */
    private fun sigmaOutput(outIdeal: Double, outActual: Double) =
        (outIdeal - outActual) * fSigmoid(outActual)

    /**
     *Расчет дельты для скрытого нейрона
     */
    private fun sigmaHidden(neuron: Neuron): Double
    {
        val outSinapses = getOutSinapses(neuron)
        val fSigmoid = fSigmoid(neuron.value)
        var sum = 0.0
        outSinapses.forEach { sum += it.value * it.neuronOut.delta }
        return sum * fSigmoid
    }

    /**
     *
     */
    private fun grad(a: Double, b: Double) =
            a * b

    /**
     *Функция МОР
     */
    private fun mor(grad: Double, dw: Double) =
            epsilon*grad + alfa * dw


    /*******МАТЕМАТИКА РАСЧЕТ ПО НЕЙРОСЕТИ*******/

    /**
     * Расчет значения нейрона на основе входных синапсов
     */
    private fun countNeuronValue(inSinapses: ArrayList<Sinaps>):Double
    {
        var h1 = 0.0
        inSinapses.forEach {
            h1 = h1 +it.value * it.neuronIn.value
        }
        val hOutput = activationFunction(h1)
        return hOutput
    }

    /**
     * Функция активации
     */
    private fun activationFunction(inValue: Double) =
            1.0/(1.0 + E.pow(-inValue))

    /***************РЯДЫ НЕЙРОСЕТИ***************/

    /**
     *Получить количество рядов в нейросети
     */
    private fun countRows(): Int
    {
        if(rowsCash == 0){
            var rows = 0
            neuronArray.forEach {
                if(rows < it.row)
                    rows = it.row
                rowsCash = rows + 1
            }
        }
        return rowsCash
    }

    /**
     *Получить нейроны находящиеся в выбраного ряда
     */
    private fun getNeuronsFromRow(rowNumber: Int): ArrayList<Neuron>
    {
        val neurons = ArrayList<Neuron>()
        neuronArray.forEach {
            if(rowNumber  == it.row)
                neurons.add(it)
        }
        return neurons
    }

    /**
     *Получить выходной нейрон
     */
    private fun getOutputNeuron() : Neuron
    {
        if(outNeuronCash.row == 0){
            val rows = countRows()
            outNeuronCash = getNeuronsFromRow(rows -1 )[0]
        }
        return outNeuronCash
    }

    /*********РАБОТА С МАССИВОМ СИНАПСОВ*********/

    /**
     *Получить входные синапсы нейрона
     */
    private fun getInSinapses(neuron: Neuron): ArrayList<Sinaps>
    {
        val sinapses = ArrayList<Sinaps>()
        sinapsArray.forEach {
            if(it.neuronOut.name == neuron.name)
                sinapses.add(it)
        }
        return sinapses
    }


    /**
     *Получить выходные синапсы нейрона
     */
    private fun getOutSinapses(neuron: Neuron): ArrayList<Sinaps>
    {
        val sinapses = ArrayList<Sinaps>()
        sinapsArray.forEach {
            if(it.neuronIn.name == neuron.name)
                sinapses.add(it)
        }
        return sinapses
    }
}