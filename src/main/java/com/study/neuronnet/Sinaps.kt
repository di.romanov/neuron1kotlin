package com.study.neuronnet

import com.study.neuronnet.Neuron

data class Sinaps(var name: String, var neuronIn: Neuron, var neuronOut: Neuron, var value: Double = 0.0 ) {

    var deltaValueLast = 0.0
}