package com.study

import com.study.neuronnet.Neuron
import com.study.neuronnet.NeuronNet
import com.study.neuronnet.Sinaps
import java.util.*
import kotlin.collections.ArrayList

/**
 * Класс строки таблицы куда записывается результат серии итераций.
 * a, b - входные значения
 * с - ожидаемое выходное значение
 * result - реальное выходное значение
 */

class ResultTableLine(val a: Double,val b:Double,val c:Double)
{
    var result = 0.0
}

//создаем нейроны
val i1 = Neuron("I1", 0)
val i2 = Neuron("I2", 0)
val h1 = Neuron("H1", 1)
val h2 = Neuron("H2", 1)
val o1 = Neuron("O1", 2)

//создаем синапсы
val w1 = Sinaps("w1", i1, h1, 0.45)
val w2 = Sinaps("w2", i1, h2, 0.78)
val w3 = Sinaps("w3", i2, h1, -0.12)
val w4 = Sinaps("w4", i2, h2, 0.13)
val w5 = Sinaps("w5", h1, o1, 1.5)
val w6 = Sinaps("w6", h2, o1, -2.3)

//допустимая погрешность работы нейросети
val delta = 0.01

val nn = NeuronNet()

/**
 * Таблица для записи результатов последней итерации.
 */
val xorResultTable = ArrayList<ResultTableLine>()

fun main()
{
    //добавляем синапсы в объект нейросети
    nn.sinapsArray.add(w1)
    nn.sinapsArray.add(w2)
    nn.sinapsArray.add(w3)
    nn.sinapsArray.add(w4)
    nn.sinapsArray.add(w5)
    nn.sinapsArray.add(w6)

    //добавляем нейроны в объект нейросети
    nn.neuronArray.add(i1)
    nn.neuronArray.add(i2)
    nn.neuronArray.add(h1)
    nn.neuronArray.add(h2)
    nn.neuronArray.add(o1)

    //создаем таблицу записи результатов
    xorResultTable.add(ResultTableLine(0.0,0.0,0.0))
    xorResultTable.add(ResultTableLine(0.0,1.0,1.0))
    xorResultTable.add(ResultTableLine(1.0,0.0,1.0))
    xorResultTable.add(ResultTableLine(1.0,1.0,0.0))

    //первый тренировачный сет вне замкнутого цикла
    //цель сета сравнить результаты вычислений с ожидаемыми взятыми из статьи
    //https://habr.com/ru/post/312450/
    println("TEST FROM EXAMPLE")
    //задаем входные параметры
    i1.value = 1.0
    i2.value = 0.0
    //получаем результат работы сети после одного сета
    var ans = nn.runNeuron()
    println("First anser: $ans")
    //проводим обучение
    nn.teachNeuronNetItirarion(1.0, ans)
    //получаем результаты работы после обучения
    ans = nn.runNeuron()
    println("Second anser: $ans")

    //запускаем непрерывное обучение в цикле
    println("RUNING STUDY")
    nn.loger.off()
    //счетчик иттераций
    var i = 0
    //время запуска нейронки (для измерения времени нахожденгеия результата)
    var startTime = System.currentTimeMillis()
    //время запуска нейронки (для измерения времени n итераций)
    var previous = System.currentTimeMillis()
    while (true){

        if(i%1000000 == 0 && i!=0)
            nn.loger.on()

        xorResultTable.forEach {
            i1.value = it.a
            i2.value = it.b
            val ans = nn.runNeuron()
            nn.teachNeuronNetItirarion(it.c, ans)
            it.result = ans
        }
        //каждый 100000 сет выводим результы работы в консоль
        if(i%100000 == 0)
        {
            val current = System.currentTimeMillis()
            val delta = current - previous
            previous = current
            printlnItiration(i, delta)
            nn.loger.off()
            //если отклонение в рамках погрешности считаем что нейросеть обучена
            if (offset())
                break
        }
        i++
    }//while
    val stopTime = System.currentTimeMillis()
    val deltaTime = stopTime - startTime
    println("Neuron teached.\nStart time: ${Date(startTime)}\nStop time: ${Date(stopTime)}\nStudy time [s]: ${deltaTime/1000}\n" +
            "Study time [ms]: $deltaTime ")
}

/**
 * Мтод распечатывает текущую итерацию
 * W - ожидаемы результат
 * H - результат выданый нейросетью
 * @param i (Itiration) текущая итерация
 * @param delta  (delta) время прошедшее с предидущей печати результов
 */
private fun printlnItiration(i: Int, delta:Long)
{
    println("Itiration: $i" +
            " W: ${xorResultTable[0].c} H: ${xorResultTable[0].result}" +
            " W: ${xorResultTable[1].c} H: ${xorResultTable[1].result}" +
            " W: ${xorResultTable[2].c} H: ${xorResultTable[2].result}" +
            " W: ${xorResultTable[3].c} H: ${xorResultTable[3].result}" +
            " deltaTime: $delta")
}

/**
 * Проверят обучилась ли нейронка
 */
fun offset(): Boolean
{
    xorResultTable.forEach {
        if(it.c < it.result -delta || it.c > it.result + delta)
            return false
    }
    return true
}
