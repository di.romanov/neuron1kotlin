package com.study

class Loger {

    private var enable = true

    fun on()
    {
        enable = true
    }

    fun off()
    {
        enable = false
    }

    fun log(data : String) {
        if (enable)
            println(data)
        else
            return
    }
}